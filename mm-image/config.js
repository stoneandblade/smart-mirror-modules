/* Magic Mirror Config Sample
 *
 * By Michael Teeuw https://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information on how you can configure this file
 * See https://github.com/MichMich/MagicMirror#configuration
 *
 */

var config = {
	address: "localhost", 	// Address to listen on, can be:
							// - "localhost", "127.0.0.1", "::1" to listen on loopback interface
							// - another specific IPv4/6 to listen on a specific interface
							// - "0.0.0.0", "::" to listen on any interface
							// Default, when address config is left out or empty, is "localhost"
	port: 8080,
	basePath: "/", 	// The URL path where MagicMirror is hosted. If you are using a Reverse proxy
					// you must set the sub path here. basePath must end with a /
	ipWhitelist: ["127.0.0.1", "::ffff:127.0.0.1", "::1"], 	// Set [] to allow all IP addresses
															// or add a specific IPv4 of 192.168.1.5 :
															// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
															// or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
															// ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	useHttps: false, 		// Support HTTPS or not, default "false" will use HTTP
	httpsPrivateKey: "", 	// HTTPS private key path, only require when useHttps is true
	httpsCertificate: "", 	// HTTPS Certificate path, only require when useHttps is true

	language: "en",
	logLevel: ["INFO", "LOG", "WARN", "ERROR"],
	timeFormat: 24,
	units: "imperial",
	// serverOnly:  true/false/"local" ,
	// local for armv6l processors, default
	//   starts serveronly and then starts chrome browser
	// false, default for all NON-armv6l devices
	// true, force serveronly mode, because you want to.. no UI on this device

	modules: [
		{
			module: "alert",
		},
		/*{
			module: "weather",
			position: "top_right",
			config: {
				type: "current",
				weatherProvider: "weathergov",
				lat: "48.197750",
				lon: "-114.313461"
			}
		},
		{
			module: "weather",
			position: "top_right",
			config: {
				type: "forecast",
				weatherProvider: "weathergov",
				lat: "48.197750",
				lon: "-114.313461",
				appendLocationNameToHeader: false
			}
		},
		{
			module: "smart-mirror/crypto-prices",
			position: "middle_center",
			config: {
				coins: 'bitcoin,ethereum,yearn-finance,chainlink'
			}
		},*/
		{
			module: 'smart-mirror/todo-list',
			position: 'bottom_left',
			config: {
				hideWhenEmpty: false,
				accessToken: '7bc3c533523ed077e1cf5bdcd7fe22e7eea45a6f',
				maximumEntries: 5,
				updateInterval: 60*1000, // Update every minute
				fade: false,
				// projects and/or labels is mandatory:
				projects: [ 2253294289, 2253294294 ]
			}
		},
		{
			module: "smart-mirror/mm-image",
			config: {
				type:'video',
				url:'https://www.w3schools.com/howto/rain.mp4',
				fileType: 'video/mp4'
				//type:'image',
				//url:'https://upload.wikimedia.org/wikipedia/commons/e/e4/StarfieldSimulation.gif'
			}
		}
	]
};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") {module.exports = config;}
