Module.register("mm-image", {
	defaults: {},

	start: function() {
		this.updateDom(0);
	},

	socketNotificationReceived: function (notification, payload) {
		if (notification === "FILEDOWNLOADED") {
			this.updateDom(1000);
		}
	},

	getStyles: function () {
		return ["mm-image.css"];
	},

	getDom: function () {
		const split = this.config.url.split("/");
		const filename = split[split.length - 1];

		const oldWrapper = document.getElementById("mm-image-wrapper");
		if (oldWrapper) {
			oldWrapper.remove();
		}

		if(this.config.type === "image") {
			var imgWrapper = document.createElement("div");
			imgWrapper.id = "mm-image-wrapper";
			imgWrapper.className = "image-wrapper";
			imgWrapper.style.backgroundImage = `url("/modules/smart-mirror/mm-image/files/${filename}")`;
			document.body.appendChild(imgWrapper);
		}

		if (this.config.type === "video") {
			var wrapper = document.createElement("video");
			wrapper.id = "mm-image-wrapper";
			wrapper.className = "image-wrapper";
			wrapper.setAttribute("autoplay", "");
			wrapper.setAttribute("muted", "");
			wrapper.setAttribute("loop", "");
			wrapper.innerHTML = `<source src="/modules/smart-mirror/mm-image/files/${filename}" type="${this.config.fileType}">`;
			document.body.appendChild(wrapper);
		}
	}
});
