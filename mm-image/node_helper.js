const NodeHelper = require("node_helper");
const config = require("../../../config/config");
const fs = require("fs");
const https = require("https");
const Log = require("../../../js/logger");

module.exports = NodeHelper.create({
	start: function () {
		config.modules.forEach((module) => {
			if (module.module.endsWith(this.name)) {
				const split = module.config.url.split("/");
				const filename = split[split.length - 1];
				let fileFound = false;

				if (!fs.existsSync(__dirname + "/files")) {
					fs.mkdirSync(__dirname + "/files");
				}

				const files = fs.readdirSync(__dirname + "/files");
				files.forEach((file) => {
					if (file === filename) {
						fileFound = true;
					} else {
						fs.unlink(__dirname + `/files/${file}`, (err) => {});
					}
				});

				if (!fileFound) {
					const file = fs.createWriteStream(__dirname + `/files/${filename}`);
					https
						.get(module.config.url, (response) => {
							response.pipe(file);
							this.sendSocketNotification("FILEDOWNLOADED", {});
							file.on("finish", function () {});
						})
						.on("error", function (err) {
							fs.unlink(__dirname + `/files/${filename}`, () => {});
						});
				}
			}
		});
	}
});
