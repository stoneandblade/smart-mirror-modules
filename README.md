# SmartMirror Modules #

## Consumer Modules ##

### Clock (use default) ###

* [https://docs.magicmirror.builders/modules/clock.html](https://docs.magicmirror.builders/modules/clock.html)
### Crypto Prices ###
* [https://bitbucket.org/stoneandblade/smart-mirror-modules/src/master/crypto-prices/](https://bitbucket.org/stoneandblade/smart-mirror-modules/src/master/crypto-prices/)

### Weather (based on default) ###
* Used for weather and forecasts
* [https://bitbucket.org/stoneandblade/smart-mirror-modules/src/master/weather/](https://bitbucket.org/stoneandblade/smart-mirror-modules/src/master/weather/)

### Image-Loader ###
* Images display
* Need to load images from URL (based on remote config)?

## Internal Modules ##


## Potential Modules ##

### Sports scores ###

### Notes (use default - complements module) ###
* [https://docs.magicmirror.builders/modules/compliments.html](https://docs.magicmirror.builders/modules/compliments.html)

### News (use default) ###
* See if we can get personalized (Google Discover or iOS whatever) feeds
* Allow for RSS entry
* Provide list of common news providers

### Commute/traffic ###
* TBD

### Stocks ###
* TBD

### TODO List ###
* Backlog - (requires oAuth) Microsoft TODO [https://github.com/thobach/MMM-MicrosoftToDo/](https://github.com/thobach/MMM-MicrosoftToDo/)
* Backlog - (requires oAth) TODOist [https://github.com/cbrooker/MMM-Todoist](https://github.com/cbrooker/MMM-Todoist)

### Wifi detection ###
* TBD

### Alerts ###
* TBD

### Calendar(s) ###
* Holiday calendar
* User calendar

### QR Code (for setup) ###
* QRCode [https://github.com/evghenix/MMM-QRCode](https://github.com/evghenix/MMM-QRCode)

### Music ###
* TBD

### Pictures ###
* Option - Google Photos [https://github.com/ChrisAcrobat/MMM-GooglePhotos](https://github.com/ChrisAcrobat/MMM-GooglePhotos)

### Countdown module ###
* Option - needs styling [https://github.com/mykle1/MMM-EventHorizon](https://github.com/mykle1/MMM-EventHorizon)

### Sunrise and sunset ***
* Option - [https://github.com/mykle1/MMM-SunRiseSet](https://github.com/mykle1/MMM-SunRiseSet)

### Thermastat ###
* Backlog - requires Google registration [register](https://www.google.com/url?q=https://developers.google.com/nest/device-access/registration&sa=D&source=hangouts&ust=1607053419814000&usg=AFQjCNFMa4GmSe7jIGFpWKR621QFuwV0MA)

### Instagram ###
* Backlog - maybe some day (https://github.com/kapsolas/MMM-Instagram)[https://github.com/kapsolas/MMM-Instagram]

### Background image ###
* Display pretty background image (from pre-defined set of images)

### Display Timeout Settings ###
* Motion detection settings
* Time range
* Always on (with burn-in warning)

### Registration module ###
* Scan QR code or manually enter machine ID and security key

### Default Settings (app-wide settings) ###
* timezone
* imperial vs metric
* Fahrenheit vs Celsius


## Theme Examples (for designer) ##

* Wallberry theme [https://github.com/delightedCrow/WallberryTheme](https://github.com/delightedCrow/WallberryTheme)
* Wicked Makers (for designer) [https://www.youtube.com/watch?v=DjPGoGmO5VY](https://www.youtube.com/watch?v=DjPGoGmO5VY)
