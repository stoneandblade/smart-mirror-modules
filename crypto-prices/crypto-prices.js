Module.register("crypto-prices", {
	// Default module config.
	defaults: {
		prices: []
	},
	getTemplate: function() {
		return "price-template.njk";
	},
	getTemplateData: function() {
		return { prices: this.config.prices };
	},
	start: function() {
		let self = this;
		this.addFilters();
		this.getPrices();
		setInterval(function() {
			self.getPrices();
		}, 10000);
	},
	getPrices: function() {
		var self = this;
		var pricesRequest = new XMLHttpRequest();
		var url = "https://api.coingecko.com/api/v3/simple/price?ids=" + this.config.coins + "&vs_currencies=usd&include_24hr_change=true&include_market_cap=true";
		pricesRequest.open("GET", url, true);
		pricesRequest.onreadystatechange = function() {
			if (this.readyState === 4 && this.status === 200) {
				var response = JSON.parse(this.response);
				var priceArray = [];

				for (const property in response) {
					var currentPrice = response[property];
					priceArray.push({
						name: property,
						price: currentPrice.usd,
						marketCap: currentPrice.usd_market_cap,
						change: currentPrice.usd_24h_change
					});
				}
				self.config.prices = priceArray;
				self.config.prices.sort(function(a, b) {
					return b.marketCap - a.marketCap;
				});
				self.updateDom(0);
			}
		};
		pricesRequest.send();
	},
	addFilters() {
		this.nunjucksEnvironment().addFilter(
			"formatMarketCap",
			function(value) {
				if (value >= 1e3) {
					var units = ["k", "M", "B", "T"];

					// Divide to get SI Unit engineering style numbers (1e3,1e6,1e9, etc)
					let unit = Math.floor(((Math.floor(value)).toFixed(0).length - 1) / 3) * 3;
					// Calculate the remainder
					var num = (value / ("1e" + unit)).toFixed(2);
					var unitname = units[Math.floor(unit / 3) - 1];

					// output number remainder + unitname
					return num + unitname;
				}

				// return formatted original number
				return value.toLocaleString();
			}.bind(this)
		);

		this.nunjucksEnvironment().addFilter(
			"formatPercent",
			function(value) {
				return value.toFixed(2) + "%";
			}.bind(this)
		);

		this.nunjucksEnvironment().addFilter(
			"currency",
			function(number) {
				var decPlaces = 2,
					decSep = ".",
					thouSep = ",";
				var sign = number < 0 ? "-" : "";
				var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
				var j = (j = i.length) > 3 ? j % 3 : 0;

				return sign +
					(j ? i.substr(0, j) + thouSep : "") +
					i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
					(decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
			}.bind(this)
		);
	}
});
