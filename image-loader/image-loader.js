
Module.register('image-loader', {

	// config
	defaults: {
		img: '',
		maxWidth: '100%',
		opacity: 1
	},

	// styles
	getStyles: () => ['image-loader.css'],

	// on start
	start: function() {
		// TODO: refresh at interval
	},

	// dom generator
	getDom: function() {
		const wrapper = document.createElement('div');
		const img = document.createElement('img');
		// TODO: image path version name
		img.src = `/modules/smart-mirror/image-loader/images/${this.config.img}`;
		img.className = 'image-loader-image';
		img.style.maxWidth = this.config.maxWidth;
		img.style.opacity = this.config.opacity;
		wrapper.appendChild(img);
		return wrapper;
	}

});
